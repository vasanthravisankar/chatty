import {Component} from "react";
import React from "react";

class Profile extends Component {
  render() {
    const { currentMember: { userName, imageUrl } } = this.props;
    if (!userName || !imageUrl) {
      return (
        <div id="profile">
          <div className="wrap">
            &nbsp;
          </div>
        </div>
      )
    }
    return (
      <div id="profile">
        <div className="wrap">
          <img id="profile-img" src={imageUrl} className="online" alt="" />
          <p>{userName}</p>
        </div>
      </div>
    );
  }
}

export default Profile;
