import {Component} from "react";
import React from "react";

class Contact extends Component {
  render() {
    let { user: { userName, userId, imageUrl }, currentMember, initiatePrivateChat, activeChatRoom } = this.props
    let isCurrentUser = currentMember.userId === userId
    let isActiveChatRoom = userId === activeChatRoom ? 'active' : ''
    return (
      <li className={`contact ${isActiveChatRoom}`} onClick={() => initiatePrivateChat(userId)}>
        <div className="wrap">
          <span className="contact-status online"></span>
          <img id="profile-img" src={imageUrl} className="online" alt="" />
          <div className="meta">
            <p className="name">{userName} {isCurrentUser && '(You)'}</p>
          </div>
        </div>
      </li>
    );
  }
}

export default Contact;
