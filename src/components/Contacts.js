import {Component} from "react";
import React from "react";

import Contact from './Contact.js'

class Contacts extends Component {
  render() {
    let { users, currentMember, initiatePrivateChat, activeChatRoom } = this.props

    return (
      <div id="contacts">
        <ul>
          {users.map(user =>
            <Contact
              activeChatRoom={activeChatRoom}
              user={user}
              currentMember={currentMember}
              initiatePrivateChat={initiatePrivateChat}
              key={user.userId} />
          )}
        </ul>
      </div>
    );
  }
}

export default Contacts;
