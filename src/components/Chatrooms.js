import {Component} from "react";
import React from "react";
import _ from 'lodash';


class Chatrooms extends Component {
  render() {
    let { publicChatRooms, activeChatRoom } = this.props

    return <div style={{padding: '20px 0 0 20px'}}>Coming Soon...</div>
    return (
      <div id="chatrooms">
        <ul>
          {publicChatRooms.map(publicChatRoom =>
            <li className="chatroom">
              {_.startCase(publicChatRoom)}
            </li>
          )}
        </ul>
      </div>
    );
  }
}

export default Chatrooms;
