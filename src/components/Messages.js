import {Component} from "react";
import React from "react";
import Message from './Message.js'

class Messages extends Component {

  renderMessage(message) {
    const { currentMember, users } = this.props;
    return <Message message={message} currentMember={currentMember} users={users} key={message.timestamp} />
  }

  render() {
    const { messages } = this.props;
    return (
      <div className="messages">
        <ul>
          {messages.map(m => this.renderMessage(m))}
        </ul>
      </div>
    );
  }
}

export default Messages;
