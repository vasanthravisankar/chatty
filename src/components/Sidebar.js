import {Component} from "react";
import React from "react";

import Profile from './Profile.js'
import Contacts from './Contacts.js'
import Chatrooms from './Chatrooms.js'
import { FaUsers, FaMicrosoft } from 'react-icons/fa'
import { TiThSmall } from 'react-icons/ti'

class Sidebar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentView: 'users'
    }
  }

  switchView(view) {
    this.setState({ currentView: view })
  }

  render() {
    let { users, currentMember, initiatePrivateChat, activeChatRoom, publicChatRooms, switchRoom } = this.props

    return (
      <div id="sidepanel">
        <Profile currentMember = {currentMember}/>
        <div className="active-chat-room-title" onClick={switchRoom.bind(this, 'main_chat_room')}>
          <i><FaMicrosoft /></i><p>Main Chat Room</p>
        </div>
        <div id="search" >
          <input type="text" placeholder="Search contacts..." />
        </div>
        { this.state.currentView === 'users' ? <Contacts
          users={users}
          currentMember={currentMember}
          initiatePrivateChat={initiatePrivateChat}
          activeChatRoom={activeChatRoom}/>
          : <Chatrooms publicChatRooms={publicChatRooms} activeChatRoom={activeChatRoom}/>
        }
        <div id="bottom-bar">
          <button onClick={this.switchView.bind(this, 'users')}>
            <i className="fa fa-user-plus fa-fw" aria-hidden="true"><TiThSmall/></i>
            <span>Active Users</span>
          </button>
          <button onClick={this.switchView.bind(this, 'rooms')}>
            <i className="fa fa-user-plus fa-fw" aria-hidden="true"><FaUsers/></i>
            <span>Rooms</span>
          </button>
        </div>
      </div>
    );
  }
}

export default Sidebar;
