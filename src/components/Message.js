import {Component} from "react";
import React from "react";

class Message extends Component {
  render() {
    const { text, timestamp, userId } = this.props.message;
    const { currentMember, users } = this.props;
    let imageUrl = users.find(u => u.userId === userId ).imageUrl
    const messageFromMe = userId === currentMember.userId;
    const className = messageFromMe ? "sent" : "replies";
    return (
      <li className={className}>
        <img src={imageUrl} alt="" />
        <p>{text}</p>
      </li>
    );
  }
}

export default Message;
