import {Component} from "react";
import React from "react";

class Input extends Component {
  state = {
    text: ""
  }

  onChange(e) {
    this.setState({text: e.target.value});
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state.text);
    this.setState({text: ""});
  }

  render() {
    let { placeholder, submitLabel, required, disabled } = this.props
    return (
      <div className="message-input">
        <div className="wrap">
          <form onSubmit={e => this.onSubmit(e)}>
            <input type="text"
              onChange={e => this.onChange(e)}
              value={this.state.text}
              required={required}
              placeholder={placeholder}
              disabled={disabled}
              autoFocus />
          <button disabled={disabled}>{submitLabel}</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Input;
