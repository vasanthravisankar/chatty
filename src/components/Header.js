import {Component} from "react";
import React from "react";
import _ from 'lodash'

class HeaderProfile extends Component {
  render() {
    let { chatTitle } = this.props
    return (
      <div className="header">
        <p>{_.startCase(chatTitle)}</p>
      </div>
    );
  }
}

export default HeaderProfile;
