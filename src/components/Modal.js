import React  from 'react';

const Modal = ({ handleClose, show, children, style }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName} onClick={() => handleClose && handleClose()}>
      <section className="modal-main" style={style}>
        {children}
      </section>
    </div>
  );
};

export default Modal;
