import React, { Component } from 'react';
import './App.css';

import Messages from './components/Messages.js'
import Input from "./components/Input.js"
import Modal from './components/Modal.js'

import HeaderProfile from './components/Header.js'
import Sidebar from './components/Sidebar.js'
import { Notification } from 'react-notification';

const publicChatRooms = ['main_chat_room', 'cricket', 'politics', 'cinema']

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rooms: {
        'main_chat_room': {
          messages: []
        },
        'cricket': {
          messages: [],
          members: []
        },
        'politics': {
          messages: [],
          members: []
        },
        'cinema': {
          messages: [],
          members: []
        }
      },
      showModal: true,
      messages: [],
      member: {},
      users: [],
      showNotification: false,
      notificationMessage: '',
      userNameError: false,
      showUsersModal: false,
      privateChats: [],
      activeChatRoom: 'main_chat_room',
      showPrivateChat: false,
      activePublicRoom: 'main_chat_room'
    }

    this.handleMessages = this.handleMessages.bind(this)
    this.initiatePrivateChat = this.initiatePrivateChat.bind(this)
  }

  componentWillUnmount() {
    this.closeConnection(this.state.member.userId)
  }

  addNotification(notificationMessage) {
    this.setState( { showNotification: true, notificationMessage })

    this.dismissNotification()
  }

  dismissNotification() {
    setTimeout(() => this.setState( { showNotification: false, notificationMessage: '' }), 2000)
  }

  showModal = () => {
      this.setState({ showModal: true });
    };

  hideModal = () => {
    this.setState({ showModal: false });
  };

  onSendMessage = (message) => {
    if (message.length > 0) {
      let { activeChatRoom, member } = this.state
      if (activeChatRoom === 'main_chat_room') {
        message = { type: 'send_group_message', text: message, member }
      } else {
        message = { type: 'private_message', text: message, targetUserId: activeChatRoom }
      }
      this.sendMessage(message)
    }
  }

  register = (username) => {
    this.connection = new WebSocket('wss://evening-lake-46529.herokuapp.com');
    //this.connection = new WebSocket('ws://127.0.0.1:7000');

    if (username.length < 1) {
      this.setState({ userNameError: true })
      return
    }
    this.openConnection(username)
    this.hideModal()
  }

  openConnection(username) {
    this.connection.onopen = function () {
      this.connection.send(username)
    }.bind(this);

    this.handleMessages()
    //this.handleMembers()
  }

  closeConnection(userId) {
    this.sendMessage({ type: 'close_connection', data: { userId } })
    this.connection.close(200, this.state.member.userId)
  }

  handleMessages() {
    this.connection.onmessage = function (message) {
      let { type, data } = JSON.parse(message.data);
      console.log(type, data)
      switch(type) {
        case 'register':
          this.setMember(data)
        break;
        case 'message':
          this.addMessage(data)
        break;
        case 'users':
          this.updateUsers(data)
        break;
        case 'private_chat':
          this.startPrivateChat(data)
          break;
        case 'private_chat_message':
            this.receivePrivateMessage(data)
          break;
        default:
          return
      }

    }.bind(this)
  }

  receivePrivateMessage(message) {
    let { text, targetUserId } = message
    this.updateMessages(targetUserId, message)
  }

  addMessage(message) {
    this.updateMessages('main_chat_room', message)
  }

  updateMessages(chatRoom, message) {
    let { rooms } = this.state
    let room = rooms[chatRoom]
    room.messages.push(message)
    rooms[chatRoom] = room
    this.setState({ rooms: rooms})
  }

  updateUsers(users) {
    this.setState({ users })
  }

  isMemberLoggedIn() {
    return Object.keys(this.state.member).length > 0
  }

  setMember = (data) => {
    if (!this.isMemberLoggedIn()) {
      this.setState({ member: data })
    }
    this.notifyMemberJoining(data)
  }

  notifyMemberJoining(member) {
    if (member.userId === this.state.member.userId) {
      return
    }

    let message = `${member.userName} has joined the chat`
    this.addNotification(message)
  }

  handleMembers() {

  }

  initiatePrivateChat(userId) {
    if (this.state.rooms[userId]) {
      this.setState( { activeChatRoom: userId })
    } else {
      this.sendMessage({ type: 'open_private_chat', userId })
    }
  }

  sendMessage(data) {
    this.connection.send(JSON.stringify(data))
  }

  startPrivateChat(privateChat) {
    let rooms = this.state.rooms
    rooms[privateChat.targetUserId] = privateChat
    if (privateChat.isInitiator) {
      this.setState( { activeChatRoom: privateChat.targetUserId })
    }
      this.setState( { rooms: rooms })

  }

  getTitle() {
    let { users, activeChatRoom } = this.state
    let user = users.find(u => u.userId === activeChatRoom)

    if (user) {
      return user.userName
    }

    return activeChatRoom
  }

  switchRoom(room) {
    this.setState( { activeChatRoom: room })
  }

  render() {
    let { users, member, userNameError, rooms, activeChatRoom } = this.state
    let messages = rooms[activeChatRoom].messages
    let chatTitle = this.getTitle()
    return (
      <div className="App">
        <div id="frame">
          <Sidebar
            users={users}
            currentMember= {member}
            initiatePrivateChat={this.initiatePrivateChat}
            publicChatRooms={publicChatRooms}
            activeChatRoom={activeChatRoom}
            switchRoom={this.switchRoom.bind(this)} />
          <div className="content">
            <HeaderProfile member={member} chatTitle={chatTitle} />
            <Messages messages={messages} currentMember={member} users={users} />
            <Input
              placeholder="Enter your message and press ENTER"
              onSubmit={this.onSendMessage}
              submitLabel="Send"
              disabled={!this.isMemberLoggedIn()} />
          </div>
        </div>
        <Modal show={this.state.showModal}>
            <p className="modal-title">Register for the chat room</p>
            <Input
              placeholder="Enter a displayname"
              onSubmit={this.register}
              submitLabel="Register"
              hasError={userNameError}
              errorMessage="Enter a valid user name"
              required={true}
            />
        </Modal>
        <Notification
          isActive={this.state.showNotification}
          message={this.state.notificationMessage}
        />
      </div>
    );
  }
}

export default App;
